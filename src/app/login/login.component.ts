import { ServiceService } from 'src/app/service/user/service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { Utils } from 'src/utilities/Utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  public correo: string;
  private name: string;
  private phone: string;
  private photo: string;

  constructor( private route: Router, public alertController: AlertController, private userService: ServiceService) { }

  ngOnInit() {
    localStorage.setItem( environment.application.name_email , '');
    localStorage.setItem( environment.application.phone_email , '');
    localStorage.setItem( environment.application.photo_email , '');
    this.correo = '';
    this.name = '';
    this.phone = '';
    this.photo = '';
  }

  public async butonSms(): Promise<void>{
    if (Utils.ValidateEmail(this.correo)) {
      this.sendTwoStep();
    } else {
      const alert = await this.alertController.create({
        header: 'Error',
        message: 'Debes digitar un correo válido.',
        buttons: ['OK']
      });
      await alert.present();
    }
  }
// Conexion con Google
  public loginWithGoogle(): void {
    this.userService.loginWithGoogle().then(
      (credential) => {
        console.log(credential);
        this.correo = credential.user.email;
        this.name = (credential.user.displayName ) ? credential.user.displayName : '';
        this.phone = (credential.user.phoneNumber) ? credential.user.phoneNumber : '';
        this.photo = (credential.user.photoURL) ? credential.user.photoURL : '';
        this.sendTwoStep();
      }
    );
  }

  // Conexion con Miicrosoft
  public loginWithMicrosoft(): void {
    this.userService.loginWithMicrosoft().then(
      (credential) => {
        console.log(credential);
        this.correo = credential.user.email;
        this.name = (credential.user.displayName ) ? credential.user.displayName : '';
        this.phone = (credential.user.phoneNumber) ? credential.user.phoneNumber : '';
        this.photo = (credential.user.photoURL) ? credential.user.photoURL : '';
        this.sendTwoStep();
      }
    );
  }

  private sendTwoStep() {
    const isPhone = localStorage.getItem(environment.application.isRegister) || '';
    const isName = localStorage.getItem(environment.application.name_email) || '';
    const isPhoneMail = localStorage.getItem(environment.application.name_email) || '';
    const isPhotoMail = localStorage.getItem(environment.application.photo_email) || '';

    if ( isName === '' || isPhoneMail === '' || isPhotoMail === '') {
      localStorage.setItem( environment.application.name_email , btoa(this.name) );
      localStorage.setItem( environment.application.phone_email , btoa(this.phone));
      localStorage.setItem( environment.application.photo_email , btoa(this.photo));
    }
    if (isPhone === '' ) {
      this.sendLogin();
    } else {
      const isEmail = localStorage.getItem(environment.application.email) || '';
      if (isEmail === this.correo){
        this.route.navigateByUrl('menu');
      }else{
        this.sendLogin();
      }
    }
  }

  private sendLogin(){
    localStorage.setItem(environment.application.email , this.correo);
    this.route.navigateByUrl('login-sms');
  }
}

