import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-init',
  templateUrl: './profile-init.page.html',
  styleUrls: ['./profile-init.page.scss'],
})
export class ProfileInitPage implements OnInit {

  public IsEditable = true;

  constructor() { }

  ngOnInit() {
  }

}
