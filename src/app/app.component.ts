import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private afMessaging: AngularFireMessaging
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.listen();
    });
  }

  listen() {
    this.afMessaging.requestPermission.subscribe(
      () => {
        console.log('requestPermission' );
        this.afMessaging.requestToken .subscribe(
        (token) => {
          console.log('Permission granted! Save to the server!', token);
          
        },
        (error) => { console.error('requestToken' , error); }
      );
        // this.afMessaging.messages.subscribe(
        //   (message) => { console.log(message); },
        //   (error) => {
        //     console.error(error);
        //   }
        // );
      },
      (error) => {
        console.error(error);
      },
    );
  }
}
