import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginSMSComponent } from './login-sms/login-sms.component';
import { MessageComponent } from './message/message.component';
import { ProfileEditionComponent } from './profile-edition/profile-edition.component';
import { BrowserModule } from '@angular/platform-browser';

const routes: Routes = [
  {
    path: '',
    component:LoginComponent
  },
  {
    path: 'login-sms',
    component: LoginSMSComponent
  },
  {
    path: 'menu',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'message',
    component: MessageComponent
  },  {
    path: 'profile-init',
    loadChildren: () => import('./profile-init/profile-init.module').then( m => m.ProfileInitPageModule)
  },

];
@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
