import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { map } from 'rxjs/operators';
import { auth } from 'firebase';
import { IQuery } from 'src/models/i.chat';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(private firestore: AngularFirestore , private firestoreAuth: AngularFireAuth) { }

  public getDataWithId(id: string , collection: string): Observable<any> {
    console.log('id ' , id);
    console.log('collection ' , collection);
    return this.firestore.collection(collection).doc(id).snapshotChanges().pipe(map(action => {
      console.log('playload ' ,action.payload.data());
      let data = action.payload.data();
      data['id'] = action.payload.id;
      return data ;
    }));
  }
// Crear servicio de integracion con firebase 
  public getAllDocuments(collection: string): Observable<any[]> {
    console.log('collection ' , collection);
    return this.firestore.collection(collection).snapshotChanges()
    .pipe(
      map((snaps) => 
      snaps.map((snap) => {
          return {
            id: snap.payload.doc.id,
            ...(snap.payload.doc.data() as {})
          };
        }
      ))
    );
  }
// Login sms datos
  public sendAuthSmsFirebase(number: string , verifier: any): Promise<any> {
    return this.firestoreAuth.auth.signInWithPhoneNumber(number , verifier );
  }
// Loguin sms Google
  public loginWithGoogle(): Promise<any>{
    const provider = new auth.GoogleAuthProvider();
    return this.firestoreAuth.auth.signInWithPopup(provider);
  }
// Login with Microsoft
  public loginWithMicrosoft(): Promise<any>{
    const provider = new auth.OAuthProvider('microsoft.com');
    return this.firestoreAuth.auth.signInWithPopup(provider);
  }

  public setData(object: any , collection: string): Promise<any>{
    return this.firestore.collection(collection).doc(object.id).set(object);
  }

  public findDataOneParameter(collection: string , query: IQuery): Observable<any[]>{
    return this.firestore.collection(collection , ref => ref.where(query.idField, '==', query.value) ).valueChanges();
  }

  public findDataParameter(collection: string , query: IQuery): Observable<any[]>{
    console.log('query' , query);
    return this.firestore
      .collection(collection , ref => ref.where( query.idField , query.compare , query.value))
      .snapshotChanges().pipe(
        map((snaps) => snaps.map((snap) => {
            console.log('snap.payload.doc.id',snap.payload.doc.id);
            return {
              id: snap.payload.doc.id,
              ...(snap.payload.doc.data() as {})
            };
          }
        ))
      );
  }


  public setDataWithoutId(collection: string ,  object: any ): Promise<any>{
    console.log('Object ' , object);
    console.log('Collection ' , collection);
    return this.firestore.collection(collection).add(object);
  }
}
