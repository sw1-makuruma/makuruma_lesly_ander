import { IChat, IQuery, Messages } from 'src/models/i.chat';
import { environment } from 'src/environments/environment';
import { FirebaseService } from './../firebase/service.service';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

declare global {
  interface Array<T> {
    sortBy(o: T): Array<T>;
  }
}

Array.prototype.sortBy = function(p) { return this.slice(0).sort((a, b) => (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0); };

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private message: IChat;
  public messages: Subject<IChat> = new Subject();



  constructor(private service: FirebaseService) { 
    this.message = { estado: 0, fechaInicio:null,
      id: null, mensajes:[], usuarios:[] };
  }

  public searchMessages(userInput: string, userOutput: string) {
    const data: string[] = [];
    data.push(userInput);
    data.push(userOutput);

    const query: IQuery = { idField : environment.filter.users, compare: 'array-contains-any', value : data };

    this.service.findDataParameter(environment.collections.chat, query).subscribe(
      (chat1: any[]) => {
        console.log('in', chat1);
        this.validateMessage(chat1);
      }
    );
    return this.messages.asObservable();
  }

  public pushMessage(message: IChat) {
    console.log('New Chat ' , message);
    if (message.id === null) {
      message.fechaInicio = new Date();
      message.estado = 1;
      return this.service.setDataWithoutId(environment.collections.chat, message);
    } else {
      return this.service.setData(message , environment.collections.chat);
    }
  }

  public validateMessage(messagesInput: IChat[]) {
    this.message = messagesInput[0];
    this.message.mensajes = this.message.mensajes.sort((a, b) => {
      return a.fecha > b.fecha ? 1 : a.fecha < b.fecha ? - 1 : 0;
    });
    this.messages.next(this.message);
  }
}
