import { ChatService } from './../service/chat/chat.service';
import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Usuario } from 'src/models/i.usuario';
import { IChat, Messages } from 'src/models/i.chat';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {

  public userEmisor: Usuario;
  public userReceptor: Usuario;
  public messages: Messages[];
  public messageInput = '';
  public chat: IChat;

  constructor( private chatService: ChatService) {  }

  ngOnInit() {

    this.loadMessage();
  }

  private loadMessage(): void {
    this.userEmisor = {
      correo : localStorage.getItem('email_user'),
      id: '', nombre: '', perfil: '', photo: '', skills: [], telefono: 9 };

    const usuarios: string [] = [];
    usuarios.push(this.userEmisor .correo);
    usuarios.push(sessionStorage.getItem('chat_with'));

    this.chat = {
      estado : 0,
      fechaInicio: null,
      id : null,
      mensajes: [],
      usuarios
    };

    this.chatService.searchMessages( usuarios[0] , usuarios[1] ).subscribe(
      (result) => {
        console.log('result' , result);
        this.chat = result;
        this.messages = result.mensajes;
        this.scrollToBottom();
      }
    );
  }

  public sendMessage(): void {
      this.chat.mensajes.push({
        emisor: this.userEmisor.correo,
        fecha: new Date(),
        mensaje: this.messageInput
      });
      this.chatService.pushMessage(this.chat).then( console.log , console.error );
      this.messageInput = '';
  }

  scrollToBottom(): void {
      setTimeout(function () {
          var itemList = document.getElementById('chat-autoscroll');
          itemList.scrollTop = itemList.scrollHeight;
      }, 10);
}
}
