import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/models/i.usuario';
import { ServiceService } from '../service/user/service.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  
  public users: Usuario [] = [];
  public filter: string;

  constructor(private userService: ServiceService , private route : Router) {}

  ngOnInit(){
    this.filter = '';
    this.filterData();
  }
 
  public filterData(): void{
    this.userService.getAllUser().subscribe(
      (users: Usuario[]) => {
        this.users = [];
        if (this.filter && this.filter.trim() !== '') {
          this.users = users.filter(
            (user) => {
              return (
                 (user.nombre.toLowerCase().indexOf(this.filter.toLowerCase()) > -1) ||
                 (user.skills.join(' ').toLowerCase().indexOf(this.filter.toLowerCase()) > -1)
              )
            }
          );
        } else {
          this.users = users;
        }
      }
    );
  }

  public sendToChat(user: Usuario): void {
    console.log('user' , user);
    sessionStorage.setItem('chat_with' , user.correo);
    sessionStorage.setItem('chat_name' , user.nombre);
    this.route.navigateByUrl('message');
  }

  public updateList(ev: any): void {
    this.filter = ev.target.value;
    this.filterData();
  }
}
