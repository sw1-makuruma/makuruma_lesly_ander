import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { Utils } from 'src/utilities/Utils';
import * as firebase from 'firebase/app';
import { ServiceService } from '../service/user/service.service';

@Component({
  selector: 'app-login-sms',
  templateUrl: './login-sms.component.html',
  styleUrls: ['./login-sms.component.scss'],
})
export class LoginSMSComponent implements OnInit {

  public phoneNumber: string;
  recaptchaVerifier: firebase.auth.RecaptchaVerifier;

  constructor(private route: Router, public alertController: AlertController , private userService: ServiceService) { }

  ngOnInit() {
    this.phoneNumber = localStorage.getItem(environment.application.phone_email) || '';
    if (this.phoneNumber !== '') {
      this.phoneNumber = atob(this.phoneNumber);
    }
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
  }
  //PopUp validación
  public async butonMenu(): Promise<void>{
    if (Utils.ValidateClearValue(this.phoneNumber)) {

      const appVerifier = this.recaptchaVerifier;
      const phoneNumberString = '+57' + this.phoneNumber;

      this.userService.setPhoneNumber(phoneNumberString , appVerifier).then(
        async (confirmationResult) => {
          // SMS sent. Prompt user to type the code from the message, then sign the
          // user in with confirmationResult.confirm(code).
          let prompt = await this.alertController.create({
            header: 'Ingrese el código de validación',
            inputs: [{ name: 'confirmationCode', placeholder: 'Confirmation Code' }],
            buttons: [
              { text: 'Verificar',
                handler: (data) => {
                  confirmationResult.confirm(data.confirmationCode)
                  .then( (result) => {
                    // User signed in successfully.
                    console.log(result.user);
                    localStorage.setItem(environment.application.phone , this.phoneNumber);
                    this.route.navigateByUrl('profile-init');

                  }).catch( (error) => {
                    this.showAlertMessage('Error' , 'Se presentó un error al validar el código.'); 
                  });
                }
              }
            ]
          });
          await prompt.present();
        }
      );
      // localStorage.setItem(environment.application.phone , this.phoneNumber);
      // localStorage.setItem(environment.application.phone , this.phoneNumber);
      // this.route.navigateByUrl('profile-init');

    } else {
      this.showAlertMessage('Error' , 'Debes digitar un teléfono válido.');
    }
  }

//Alerta Mensaje 
  private async showAlertMessage(title: string , sms: string) {
    const alert = await this.alertController.create({
      header: title,
      message: sms,
      buttons: ['OK']
    });
    await alert.present();
  }
}
