export interface Usuario {
    id: string;
    photo: string;
    correo: string;
    nombre: string;
    perfil: string;
    skills: string[];
    telefono: number;
}

