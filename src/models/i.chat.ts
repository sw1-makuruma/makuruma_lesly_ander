import { Usuario } from './i.usuario';

// export interface Messages {
//     emisor: string;
//     receptor: string;
//     fecha: Date;
//     mensaje: string;
// }

export interface Messages {
    emisor: string;
    fecha: Date;
    mensaje: string;
}

// export interface Chat {
//     estado : number;
//     fechaFin: Date;
//     fechaInicio: Date;
//     mensajes: Messages[];
//     usuarioEmisor: Usuario;
//     usuarioReceptor: Usuario;
// }

export interface IChat {
    id: string;
    estado: number;
    fechaInicio: Date;
    mensajes: Messages[];
    usuarios: string[];
}

export interface IQuery {
    idField: string;
    compare: '==' | '=!' | 'in' | any;
    value: string[];
}