// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyBvZf7iG_9kru729ih4N3pbr2oCNtOco7s',
    authDomain: 'makuruma-3136a.firebaseapp.com',
    databaseURL: 'https://makuruma-3136a.firebaseio.com',
    projectId: 'makuruma-3136a',
    storageBucket: 'makuruma-3136a.appspot.com',
    messagingSenderId: '695459882977',
    appId: '1:695459882977:web:d251826c1b85aadbc5d4a0'
  },
  application : {
    phone: 'phone_user',
    email: 'email_user',
    isRegister: 'register_user',
    name_email : 'eman',
    phone_email: 'enohp',
    photo_email: 'otohp'
  },
  collections: {
    user: 'Usuarios',
    chat: 'Chats'
  },
  filter : {
    users : 'usuarios'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
