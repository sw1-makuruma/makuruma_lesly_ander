export class Utils {

    // TODO
    public static ValidateClearValue(input: string): boolean {
        return (input !== '');
    }
// Función para validar que el email sea extrictamente de la konrad lorenz
    public static ValidateEmail(input: string): boolean {
        let isValidEmail = false;
        if (this.ValidateClearValue(input)) {
            const regex = /^[-\w.%+]{1,64}@konradlorenz.edu.co$/i;
            isValidEmail = regex.test(input);
        }
        return isValidEmail;
    }
}